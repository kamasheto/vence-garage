package lib;

import java.io.FileNotFoundException;
import java.io.FileInputStream;
import java.util.Properties;
import java.util.Hashtable;

public class __ {
  /**
   * IO
   */
  public static void print(Object o) {
    System.out.print(o);
  }

  public static void println(Object o) {
    System.out.println(o);
  }

  public static void highlight(Object o) {
    println();
    highlight();
    println("====== " + o);
    highlight();
  }

  public static void highlight() {
    println("===================");
  }

  public static void println() {
    System.out.println();
  }

  public static int readInt() {
    print(">> ");
    int what = -1;
    try {
      what = Integer.parseInt(System.console().readLine());
    } catch (Exception e) {
      // Boom!
      // e.printStackTrace();
      return -1;
    }

    return what;
  }

  public static String readString() {
    print(">> ");
    return System.console().readLine();
  }

  public static int readPIN() {
    print(">> ");
    try {
      return Integer.parseInt(new String(System.console().readPassword()));
    } catch (Exception e) {
      return -1;
    }
  }


  /**
   * Props
   */
  private static Hashtable<String, Integer> configs;
  public static Integer getConfig(String key) {
    if (configs == null) {
      setupConfigs();
    }

    return configs.get(key);
  }

  // Source: http://crunchify.com/java-properties-file-how-to-read-config-properties-values-in-java/
  private static void setupConfigs() {
    try {
      configs = new Hashtable<String, Integer>();
      Properties prop = new Properties();
      String propFileName = "resources/config.properties";

      FileInputStream inputStream = new FileInputStream(propFileName);

      if (inputStream != null) {
        prop.load(inputStream);
      } else {
        throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
      }

      for (String key : prop.stringPropertyNames()) {
        configs.put(key, Integer.parseInt(prop.getProperty(key)));
      }
    } catch (Exception e) {
      // BOOM!
      e.printStackTrace();
    } 
  }
}