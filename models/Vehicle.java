package models;

import java.util.Date;
import lib.*;
import java.util.Hashtable;

/**
 * Vehicles can be either cars or bikes
 * Bikes will take the same space needed to park as a car (no special slots made for bikes)
 */
public class Vehicle extends Base {
  private static Hashtable<String, Vehicle> allVehicles = new Hashtable<String, Vehicle>();

  public Date parkedTime; 
  public String plate;

  public Vehicle(String plate) {
    this.plate = plate;
    allVehicles.put(plate, this);
  }

  public void park() {
    // TODO set parkedTime
  }

  public Date leave() {
    Date p = parkedTime;
    parkedTime = null;
    return p; // calculate cost?
  }

  public boolean equals(Object object) {
    if (object != null && object instanceof Vehicle) {
      return plate.equals(((Vehicle) object).plate);
    }

    return false;
  }

  public static Car getCar(String plate) {
    Vehicle v = getVehicle(plate);
    if (v == null) {
      v = new Car(plate);
    }

    if (v instanceof Car) {
      return (Car) v;
    } else {
      // Searching for bike but chose Car
      return null;
    }
  }

  public static Bike getBike(String plate) {
    Vehicle v = getVehicle(plate);
    if (v == null) {
      v = new Bike(plate);
    }

    if (v instanceof Bike) {
      return (Bike) v;
    } else {
      // Searching for a car but chose Bike
      return null;
    }
  }

  public static Vehicle getVehicle(String plate) {
    return allVehicles.get(plate);
  }

  
}