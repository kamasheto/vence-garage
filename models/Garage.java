package models;

import lib.__;
import java.util.ArrayList;

/** 
 * Singleton, since we have only one garage per app
 */
public class Garage extends Base {
  private ArrayList<Level> levels = new ArrayList<Level>();

  public Garage() {
    int total_levels = __.getConfig("total_levels");
    for (int i = 0; i < total_levels; i++) {
      levels.add(new Level());
    }
  }

  public static int totalSpots() {
    return __.getConfig("total_levels") * __.getConfig("spots_per_level");
  }

  public static int totalParked() {
    int total = 0;
    for (Level level : getLevels()) {
      total += level.vehicles.size();
    }
    return total;
  }

  public static int totalAvailable() {
    return totalSpots() - totalParked();
  }

  public static Level level(Vehicle v) {
    for (Level level : getLevels()) {
      if (level.contains(v)) {
        return level;
      }
    }

    return null;
  }

  public static Level park(Vehicle v) {
    // Just in case, 
    Level check = level(v);
    if (check != null) {
      return check;
    }

    for (Level level : getLevels()) {
      if (level.park(v)) {
        return level;
      }
    }

    return null; // couldn't park, probably all full
  }

  public static ArrayList<Level> getLevels() {
    return getGarage().levels;
  }

  private static Garage theGarage;
  public static Garage getGarage() {
    if (theGarage == null) {
      theGarage = new Garage();
    }

    return theGarage;
  }

  public static void setup() {
    getGarage(); // Just setting up the first Garage call. No need to return it
  }
}