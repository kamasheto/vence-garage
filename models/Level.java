package models;

import lib.__;
import java.util.Date;
import java.util.ArrayList;

public class Level extends Base {
  ArrayList<Vehicle> vehicles = new ArrayList<Vehicle>();

  public int spot(Vehicle v) {
    return vehicles.indexOf(v) + 1;
  }

  public boolean contains(Vehicle v) {
    for (Vehicle vehicle : vehicles) {
      if (v.equals(vehicle)) {
        return true;
      }
    }

    return false;
  }

  /**
   * @return true|false whether this car was successfully added to level (set in conf.properties)
   */
  public boolean park(Vehicle v) {
    if (vehicles.size() >= __.getConfig("spots_per_level")) {
      // Can't park here
      return false;
    }

    vehicles.add(v);
    v.park();
    return true;
  }

  /**
   * @return true|false whether this vehicle was removed from this level
   */
  public Date leave(Vehicle v) {
    if (vehicles.remove(v)) {
      // 
      return v.leave();
    }

    return null;
  }

  public String toString() {
    return "" + Garage.getLevels().indexOf(this); // no need to +1. Levels 0 do exist
  }
}