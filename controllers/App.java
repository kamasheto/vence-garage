package controllers;

import lib.__;
import models.Garage;

public class App {
  public static void main(String[] args) {
    Garage.setup();
    printIntructions();

    int choice = __.readInt();
    loop: do {
      switch (choice) {
        case 1:
          Parker.run();
          printIntructions();
          break;

        // This should be for all
        // case 2:
        //   Finder.run();
        //   printIntructions();
        //   break;

        case 2:
          Leaver.run();
          printIntructions();
          break;

        case 3:
          Manager.run();
          printIntructions();
          break;

        case 0:
          break loop;

        default:
          __.println("Didn't really get that. Try again?");
      } 

      choice = __.readInt();
    } while (choice != 0);

    __.println("Have a great day then!");
  }

  private static boolean firstTime = true;
  public static void printIntructions() {
    if (firstTime) {
      firstTime = false;
      __.println("Welcome to Vence Garage System!");
      __.println("To what do we owe the pleasure?");
      __.println();
    } else {
      __.println();
      __.println("Now what?");
    }
    __.println("1. Park in garage");
    // __.println("2. Find my vehicle");
    __.println("2. Leave garage");
    __.println("3. Manager access (requires PIN-code)");
    __.println();
    __.println("0. Exit system");
  }
}