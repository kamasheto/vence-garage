package controllers;

import lib.__;
import models.Garage;

public class Manager {
  private static boolean authenticated = false;
  
  public static boolean authenticate() {
    while (! authenticated) {
      __.println("Please enter the managers PIN code. (0 to exit)");
      int pwd = __.readPIN();
      if (pwd == 0) 
        return false;
      if (pwd == __.getConfig("manager_pin")) {
        authenticated = true;
        __.highlight("Thanks for entering your PIN!");
      } else {
        __.println("Incorrect PIN!");
      }
    }
    return true;
  }

  public static void run() {
    if (! authenticate()) {
      return;
    }

    __.println("So Mr. Manager, what do you need?");

    boolean keepGoing = false;
    do {
      __.println("1. Find a car");
      __.println("2. Garage statistics");

      keepGoing = false;
      int choice = __.readInt();
      switch (choice) {
        case 1:
          Finder.run();
          break;

        case 2:
          statistics();
          break;

        default:
          keepGoing = true;
          __.println("Didn't really get that. What?");
      }

    } while (keepGoing);
  } 

  public static void statistics() {
    __.highlight();
    __.println("Total garage spots: " + Garage.totalSpots());
    __.println("Total cars parked: " + Garage.totalParked());
    __.println("Total spots available: " + Garage.totalAvailable());
    __.highlight();
  }
}