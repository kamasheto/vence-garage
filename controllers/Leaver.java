package controllers;

import lib.__;
import models.*;

public class Leaver {
  public static void run() {
    __.println("So you'd like to leave!");

    boolean tryAgain;
    do {
      tryAgain = false;
      Vehicle v;
      String plate;    
      __.println("What did you park?");
      __.println("1. Car");
      __.println("2. Bike");

      int thing = __.readInt();
      switch(thing) {
        case 1: 
          __.println("What is your car registration plate numbers?");
          if (! leave(Vehicle.getCar(__.readString()))) {
            tryAgain = true;
          }
          break;

        case 2:
          __.println("What is your bike registration plate numbers?");
          if (! leave(Vehicle.getBike(__.readString()))) {
            tryAgain = true;
          }
          break;

        default:
          tryAgain = true;
          break;
      }
    } while (tryAgain);
  } 

  public static boolean leave(Vehicle v) {
    if (v == null) {
      __.highlight("Are you sure you're looking for what you told us you're looking for?");
      return false;
    }

    Level level = Garage.level(v);
    if (level != null) {
      level.leave(v);
      __.highlight("Your vehicle has been dispatched. Good bye!");
    } else {
      __.highlight("You don't seem to be parked with us today to begin with!");
    }
    return true;
  }
}