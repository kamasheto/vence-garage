package controllers;

import lib.__;
import models.*;

public class Parker {
  public static void run() {
    __.println("So you'd like to park!");

    boolean tryAgain;
    do {
      tryAgain = false;
      Vehicle v;
      String plate;    
      __.println("What are you riding today?");
      __.println("1. Car");
      __.println("2. Bike");

      int thing = __.readInt();
      switch(thing) {
        case 1: 
          __.println("What is your car registration plate numbers?");
          if (! park(Vehicle.getCar(__.readString()))) {
            tryAgain = true;
          }
          break;

        case 2:
          __.println("What is your bike registration plate numbers?");
          if (! park(Vehicle.getBike(__.readString()))) {
            tryAgain = true;
          }
          break;

        default:
          tryAgain = true;
          break;
      }
    } while (tryAgain);
  } 

  public static boolean park(Vehicle v) {
    if (v == null) {
      __.highlight("Are you sure you're looking for what you told us you're looking for?");
      return false;
    }

    Level level = Garage.level(v);
    if (level != null) {
      __.highlight("You seem to be already parked in level: " + level + ", spot: " + level.spot(v));
    } else {
      level = Garage.park(v);
      if (level != null) {
        __.highlight("Your vehicle has been parked in level: " + level + ", spot: " + level.spot(v));
      } else {
        __.highlight("We're sorry but the garage is currently full.");
      }
    }
    return true;
  }
}