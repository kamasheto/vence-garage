package controllers;

import lib.__;
import models.*;

public class Finder {
  public static void run() {
    boolean tryAgain;
    do {
      tryAgain = false;
      Vehicle v;
      String plate;    
      __.println("What are you looking for?");
      __.println("1. Car");
      __.println("2. Bike");

      int thing = __.readInt();
      switch(thing) {
        case 1: 
          __.println("What is your car registration plate numbers?");
          if (! find(Vehicle.getCar(__.readString()))) {
            tryAgain = true;
          }
          break;

        case 2:
          __.println("What is your bike registration plate numbers?");
          if (! find(Vehicle.getBike(__.readString()))) {
            tryAgain = true;
          }
          break;

        default:
          tryAgain = true;
          break;
      }
    } while (tryAgain);     
  } 

  public static boolean find(Vehicle v) {
    if (v == null) {
      __.highlight("Are you sure you're looking for what you told us you're looking for?");
      return false;
    }

    Level level = Garage.level(v);
    if (level != null) {
      __.highlight("You are parked in level: " + level + ", spot: " + level.spot(v));
    } else {
      __.highlight("We're afraid we couldn't find that car parked here.");
    }
    return true;
  }
}